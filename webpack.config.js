const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const plugins = {
    devServer: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'Test Crystal',
            template: path.join(__dirname, 'templates/index.html'),
        }),
        new ScriptExtHtmlWebpackPlugin({
            inline: ['bundle.js']
        }),
        new webpack.DefinePlugin({
            PRODUCTION: JSON.stringify(false)
        })
    ],

    production: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'Test Crystal',
            template: path.join(__dirname, 'templates/index.html'),
        }),
        new ScriptExtHtmlWebpackPlugin({
            inline: ['bundle.js']
        }),
        new webpack.DefinePlugin({
            PRODUCTION: JSON.stringify(false),
            'process.env.PRODUCTION': JSON.stringify(true)
        }),
        new UglifyJSPlugin({
            uglifyOptions: {
                warnings: false,
                compress: {
                    properties: false
                }
            }
        }),
        new webpack.DefinePlugin({
            PRODUCTION: JSON.stringify(true)
        })
    ]
};

module.exports = function(env, args) {
    return {
        entry: {
            app: './src/index.js'
        },

        output: {
            path: path.resolve(__dirname, 'public'),
            filename: 'bundle.js'
        },

        devtool: env.mode === 'devServer' ? 'inline-source-map' : '',

        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    include: path.resolve(__dirname, 'src/'),
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['env']
                        }
                    }
                },
                {
                    test: /\.(jpe?g|png|ttf|eot|svg|mp3|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                    use: 'base64-inline-loader?name=[name].[ext]'
                },
            ]
        },
        plugins: plugins[env.mode],

        devServer: (env.mode === 'devServer') ? {
                contentBase: path.join(__dirname, 'public'),
                compress: false,
                port: 8080,
                inline: true,
                overlay: true
        } : {},
        watch: env.mode === 'devServer',
    };
}