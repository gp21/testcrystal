import * as PIXI from 'pixi.js';
import {GameField} from "./components/game-field";
import SwipeHelper from "../helpers/swipe-helper";

class GameView extends PIXI.Container {
    constructor(resources, fieldConfig) {
        super();

        console.log('Init GameView');

        this.resources = resources;
        this.fieldConfig = fieldConfig;

        this.createGameField();

        this.swipeHelper = new SwipeHelper(this);
        this.swipeHelper.registerMultiple(this.gamefield.tiles);
        this.swipeHelper.onSwipe((target, direction) => this.onSwipe(target, direction));

        this.createDebugCenter();
    }

    createGameField() {
        this.gamefield = new GameField(this.resources, this.fieldConfig);

        this.addChild(this.gamefield);

        console.log('VIEW FIELD CREATED')
    }

    onSwipe(target, direction) {
        this.gamefield.attemptMove(target, direction);
    }

    createDebugCenter() {
        const debugCenter = new PIXI.Graphics();
        debugCenter.beginFill(0x00ff00);
        debugCenter.drawCircle(0, 0, 5);
        debugCenter.endFill();

        // DEBUG PURPOSE ONLY, represents center of the container
        debugCenter.visible = false;

        this.addChild(debugCenter);
    }

    update() {
        if (this.gamefield) {
            this.gamefield.update();
        }
    }
}

export {
    GameView
};