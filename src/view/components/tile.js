import * as PIXI from 'pixi.js';
import * as TWEEN from 'tween.js/src/Tween.js';

const tileShakeRadius = 10;
const tileShakeAmount = 10;
const tileSwapDuration = 400;

class Tile extends PIXI.Container {
    constructor(resources, rowIndex, colIndex, iconSize, tileSize, tintColor) {
        super();

        this.resources = resources;

        this.interactive = true;

        this.isMoving = false;

        this.row = rowIndex;
        this.col = colIndex;
        this.iconSize = iconSize;
        this.tileSize = tileSize;
        this.tintColor = tintColor;

        this.createSprite();

        this.createHitZone();
    }

    createHitZone() {
        const hitZone = new PIXI.Graphics();
        hitZone.beginFill(0xFff00);
        hitZone.drawRect(-this.tileSize * .5, -this.tileSize * .5, this.tileSize, this.tileSize);
        hitZone.endFill();

        hitZone.alpha = 0;
        this.addChild(hitZone)
    }

    createSprite() {
        this.sprite = new PIXI.Sprite(this.resources.gem.texture);
        this.sprite.width = this.iconSize;
        this.sprite.height = this.iconSize;
        this.sprite.anchor.set(0.5, 0.5);
        this.sprite.tint = Math.random() * this.tintColor;

        this.addChild(this.sprite);
    }

    startShaking() {
        this.shakeCount = tileShakeAmount;
        this.shake();
    }

    shake() {
        const angle = Math.random() * Math.PI * 2;
        const radius = tileShakeRadius;
        const dX = radius * Math.cos(angle);
        const dY = radius * Math.sin(angle);

        this.shakeTween = new TWEEN.Tween(this.sprite.position)
            .to({ x: dX, y: dY }, 5)
            .easing(TWEEN.Easing.Sinusoidal.Out)
            .yoyo(true)
            .repeat(1)
            .onComplete(()=>{
                this.shakeCount --;
                if (this.shakeCount > 0) {
                    this.shake();
                }
            })
            .start();
    }

    tweenTo(targetX, targetY, newCol, newRow) {
        this.isMoving = true;

        this.moveTween = new TWEEN.Tween(this.position)
            .to({ x: targetX, y: targetY }, tileSwapDuration)
            .easing(TWEEN.Easing.Sinusoidal.InOut)
            .onComplete(()=>{
                this.onMovedToNewPos(newCol, newRow);
            })
            .start();
    }

    onMovedToNewPos(newCol, newRow) {
        this.col = newCol;
        this.row = newRow;

        this.isMoving = false;
    }

    stopShaking() {
        if (this.shakeTween) {
            this.shakeTween.stop();
        }
        this.sprite.position.set(0, 0);
    }

    update() {
        TWEEN.update();
    }
}

export {Tile};