import * as PIXI from 'pixi.js';
import {Tile} from "./tile";
import MoveTypeEnum from "../../enums/move-type-enum";
import {SoundManager} from "../../utils/sound-manager";

class GameField extends PIXI.Container{
    constructor(resources, fieldConfig) {
        super();

        this.resources = resources;

        this.tileSize = fieldConfig.tileSize;
        this.tileIconSize = fieldConfig.tileIconSize;

        this.columnAmount = fieldConfig.gridSize.width;
        this.rowAmount = fieldConfig.gridSize.height;

        this.cellBorderColor = fieldConfig.cellBorderColor;
        this.cellBgColor = fieldConfig.cellBgColor;
        this.cellBgAlpha = fieldConfig.cellBgAlpha;

        this.createBackground();
        this.createTiles();

        this.createDebugCenter();
    }

    createBackground() {
        this.backgroundWrapper = new PIXI.Container();
        this.addChild(this.backgroundWrapper);

        // const { width, height } = this.tileSize;
        const heightTotal = this.rowAmount * this.tileSize;

        this.stepX = this.tileSize;
        this.stepY = this.tileSize;
        this.posStartY = (this.tileSize - heightTotal) * 0.5;

        for (let i = 0; i < this.rowAmount; i++) {
            const posY = this.posStartY + i * this.stepY;
            this.createGridRow(posY);
        }
    }

    createGridRow(posY) {
        const widthTotal = this.stepX * this.columnAmount;
        this.posStartX = (this.stepX - widthTotal) * 0.5;

        for (let i = 0; i < this.columnAmount; i++) {
            const background = this.createCellBackground();
            const posX = this.posStartX + i * this.stepX;
            background.position.set(posX, posY);

            this.backgroundWrapper.addChild(background);
        }
    }

    createCellBackground() {
        const cell = new PIXI.Container();

        const backgroundGraphics = new PIXI.Graphics();
        backgroundGraphics.beginFill(this.cellBgColor, this.cellBgAlpha);
        backgroundGraphics.drawRect(-this.tileSize * 0.5, -this.tileSize * 0.5, this.tileSize, this.tileSize);
        backgroundGraphics.endFill();

        cell.addChild(backgroundGraphics);

        const borderGraphics = new PIXI.Graphics();
        borderGraphics.beginFill(0x000, 0);
        borderGraphics.lineStyle(2, this.cellBorderColor);
        borderGraphics.drawRect(-this.tileSize * 0.5, -this.tileSize * 0.5, this.tileSize, this.tileSize);
        borderGraphics.endFill();

        cell.addChild(borderGraphics);

        return cell;
    }

    createTiles() {
        this.tiles = [];

        for (let row = 0; row < this.rowAmount; row++) {
            const posY = this.posStartY + this.stepY * row;
            this.createTileRow(row, posY);
        }
    }

    createTileRow(rowIndex, posY) {
        for (let col = 0; col < this.columnAmount; col++) {
            const posX = this.posStartX + this.stepX * col;

            const randomTint = Math.random() * 0xFFFFFF;
            const tile = new Tile(this.resources, rowIndex, col, this.tileIconSize, this.tileSize, randomTint);
            tile.position.set(posX, posY);
            this.addChild(tile);

            this.tiles.push(tile);
        }
    }

    attemptMove(targetTile, direction) {
        if (targetTile.isMoving) {
            return;
        }

        const neighbour = this.getNeighbour(targetTile, direction);
        if (neighbour && !neighbour.isMoving) {
            this.moveTiles(targetTile, neighbour);
        } else {
            this.showWrongMove(targetTile);
        }
    }

    showWrongMove(tile) {
        console.log('WRONG MOVE!');
        tile.startShaking();

        SoundManager.playSound(this.resources.audio.buzzer);
    }

    getNeighbour(targetTile, direction) {
        switch (direction) {
            case MoveTypeEnum.MOVE_UP:
                if (targetTile.row !== 0) {
                    return this.getTileByGridPos(targetTile.col, targetTile.row - 1);
                }
                break;
            case MoveTypeEnum.MOVE_DOWN:
                if (targetTile.row !== this.rowAmount - 1) {
                    return this.getTileByGridPos(targetTile.col, targetTile.row + 1);
                }
                break;
            case MoveTypeEnum.MOVE_LEFT:
                if (targetTile.col !== 0) {
                    return this.getTileByGridPos(targetTile.col - 1, targetTile.row);
                }
                break;
            case MoveTypeEnum.MOVE_RIGHT:
                if (targetTile.col !== this.columnAmount - 1) {
                    return this.getTileByGridPos(targetTile.col + 1, targetTile.row);
                }
                break;
            default:
                return null;
        }
    }

    getTileByGridPos(col, row) {
        return this.tiles.find((element) => element.col === col && element.row === row);
    }

    moveTiles(tile, neighbour) {
        const { x: neighbourX, y: neighbourY } = neighbour.position;
        const { x: tileX, y: tileY } = tile.position;
        tile.tweenTo(neighbourX, neighbourY, neighbour.col, neighbour.row);
        neighbour.tweenTo(tileX, tileY, tile.col, tile.row);

        SoundManager.playSound(this.resources.audio.whoosh);
    }

    createDebugCenter() {
        const debugCenter = new PIXI.Graphics();
        debugCenter.beginFill(0x00ff00);
        debugCenter.drawCircle(0, 0, 5);
        debugCenter.endFill();

        // DEBUG PURPOSE ONLY, represents center of the container
        debugCenter.visible = false;

        this.addChild(debugCenter);
    }

    update() {
        if (this.tiles) {
            this.tiles.forEach((tile) => tile.update());
        }
    }
}

export {GameField};