import * as PIXI from 'pixi.js';
import {Resizer} from "./utils/resizer";
import {GameController} from "./controller/game-controller";

const HEIGHT = 960;
const WIDTH = 960;

class Game extends PIXI.Container {
    constructor() {
        super();

        const app = this.initPixi();

        const resizer = new Resizer(app, WIDTH, HEIGHT);

        new GameController(app);
    }

    initPixi() {
        const app = new PIXI.Application({
            width: window.innerWidth,
            height: window.innerHeight,
            autoResize: true,
            backgroundColor: 0x000000,
            resolution: devicePixelRatio || 1,
        });

        document.body.appendChild(app.view);

        return app;
    }
}

export {
    Game,
    WIDTH,
    HEIGHT
};