'use strict';

class Resizer {
    constructor(app, gameWidth, gameHeight) {
        this._width = gameWidth;
        this._height = gameHeight;

        this._gameWidth = gameWidth;
        this._gameHeight = gameHeight;

        this._isPortrait = true;

        this.callbacks = {};

        console.log('init resizer');

        this.app = app;
        this.view = app.view;

        window.addEventListener('resize', () => {
            this.fitRendererToWindow();
        });

        this.fitRendererToWindow();
    }

    onResize(callback) {
        this.callbacks[callback] = callback;
    }

    get width() {
        return this._width;
    }

    get height() {
        return this._height;
    }

    get isPortrait() {
        return this._isPortrait;
    }

    fitRendererToWindow() {
        this._width = window.innerWidth;
        this._height = window.innerHeight;

        // landscape
        if (window.innerWidth > window.innerHeight) {
            this._isPortrait = false;

            const scale = (window.innerWidth / this._gameWidth);
            this.app.stage.scale.set(scale, scale);
        }
        else { // portrait
            this._isPortrait = true;
            const scale = (window.innerHeight / this._gameHeight);
            this.app.stage.scale.set(scale, scale);
        }

        this.app.stage.x = window.innerWidth * .5;
        this.app.stage.y = window.innerHeight * .5;

        for (let key in this.callbacks) {
            const callback = this.callbacks[key];
            callback({ width: this._width, height: this._height, isPortrait: this._isPortrait });
        }

        this.app.renderer.resize(window.innerWidth, window.innerHeight);
    }
}

export {
    Resizer
};