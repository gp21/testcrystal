class GameModel {
    get fieldConfig() {
        return {
            tileSize: 55,
            tileIconSize: 50,
            gridSize: { width: 8, height: 8 },
            cellBorderColor: 0x323744,
            cellBgColor: 0x686854,
            cellBgAlpha: 0.5,
        }
    }
}

export {GameModel};