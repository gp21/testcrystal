import {GameView} from "../view/game-view";
import {GameModel} from "../model/game-model";
import * as PIXI from "pixi.js";

class GameController {
    constructor(app) {
        this.app = app;

        app.ticker.add(this.update, this);

        this.init();
    }

    init() {
        new Promise(this.loadImageAssets.bind(this))
            .then(this.loadAudioAssets.bind(this))
            .then(this.initModels.bind(this))
            .then(this.startGame.bind(this));
    }

    initModels() {
        this.model = new GameModel();
    }

    loadImageAssets(resolve, reject) {
        const loader = new PIXI.Loader();
        loader
            .add('gem', require('../../assets/images/gem.png'))
            .load((loader, resources) => {
                this.resources = resources;

                resolve();
            })
    }

    loadAudioAssets() {
        return new Promise((resolve, reject) => {
            const load = require('audio-loader');
            load ({
                buzzer: require('../../assets/audio/buzzer.mp3'),
                whoosh: require('../../assets/audio/whoosh.mp3')
            })
                .then((dataLoaded) => {
                    this.resources.audio = dataLoaded;

                    resolve();
                })
        })
    }

    startGame() {
        this.gameView = new GameView(this.resources, this.model.fieldConfig);

        this.app.stage.addChild(this.gameView);
    }

    update() {
        if (this.gameView) {
            this.gameView.update();
        }
    }
}

export {GameController};